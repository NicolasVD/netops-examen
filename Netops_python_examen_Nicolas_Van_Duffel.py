"""
Opdracht:

Bepalingen:
 - Je moet gebruik maken van de aangeleverde variable(n)
 - Je mag deze variable(n) niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Inlever datum is zondag avond 13 maart 2022 18:00CET
 - Als inlever formaat wordt een git url verwacht die gecloned kan worden

/ 5 ptn 1 - Maak een public repository aan op jouw gitlab account voor dit project
/10 ptn 2 - Gebruik python om de gegeven api url aan te spreken
/20 ptn 3 - Gebruik regex om de volgende data te extracten:
    - Jaar, maand en dag van de created date
    - De provider van de nameservers (bijv voor 'ns3.combell.net' is de provider 'combell')
/15 ptn 4 - Verzamel onderstaande data en output alles als yaml. Een voorbeeld vind je hieronder.
    - Het land van het domein
    - Het ip van het domain
    - De DNS provider van het domein
    - Aparte jaar, maand en dag van de created date


Totaal  /50ptn
"""

""" voorbeeld yaml output

created:
  dag: '18'
  jaar: '2022'
  maand: '02'
ip: 185.162.31.124
land: BE
provider: combell

"""
#imports
import re
import requests
import json
#basis URL waaruit de data moet komen
api_base_url = "https://api.domainsdb.info/v1/domains/search?domain=syntra.be"
#GET request voor uw data
response = requests.get(f"{api_base_url}/broadcast_messages")
#response data van het GET request in 1 variabele zetten
response_data = response.json()
#variabele maken om mee te starten
yamlfile = ""
#door 'domains' cyclen 
for i in response_data["domains"]:
    #regex magie
    yamlfile += "created:\n"
    date = re.findall("[0-9]{4}-[0-9]{2}-[0-9]{2}", "2022-02-18T05:46:38.026230")[0]
    s = date.split("-")
    #de date toevoegen aan uwe yaml
    yamlfile += f"  dag: '{s[1]}'\n" 
    yamlfile += f"  jaar: '{s[0]}'\n"
    yamlfile += f"  maand: '{s[2]}'\n"
    #andere dinges toevoegen
    yamlfile += f"ip: {i['A'][0]}\n"
    yamlfile += f"country: {i['country']}\n"
    yamlfile += f"provider: {i['NS'][0].split('.')[1]}"
#output printen
print(yamlfile)